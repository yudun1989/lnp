#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
int main(void){
    int fd = -1;
    char filename[] = "test.txt";
    fd = open(filename, O_RDWR|O_CREAT|O_EXCL, S_IRWXU);
    if (-1 == fd){
        printf("open file %s failure ! fd:%d\n", filename, fd);
        fd = open(filename, O_RDWR);
        printf("fd %d\n", fd);
    }else {
        printf("open file %s success ! fd:%d\n", filename, fd);
    }
    return 0;
}
