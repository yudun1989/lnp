#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
int main(void){
    off_t offset = -1;
    offset = lseek(1, 0, SEEK_CUR);//将标准输入文件描述符的文件偏移量设为当前值
    if (-1 == offset) {
        printf("STDIN CANT' SEEK\n");
        return -1;
    }else {
        printf("stdin can seek \n");
    }
    return 0;
}
