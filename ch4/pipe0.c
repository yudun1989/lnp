#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
int main(void){
    int result = -1;
    int fd[2];
    pid_t pid;
    int *write_fd = &fd[1];
    int *read_fd = &fd[0];
    result = pipe(fd);
    if(-1 == result){
        printf("建立管道失败\n");
        return -1;
    }
    pid = fork();
    if(-1 == pid) {
        printf("fork 进程失败\n");
        return -1;
    }
    if (0 == pid){
        close(*read_fd);
    }else {
        close(*write_fd);
    }
    return 0;
}
