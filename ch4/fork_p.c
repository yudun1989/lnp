#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
int main(void){
    pid_t pid;
    pid = fork();
    if(-1 == pid){
        printf("进程创建失败\n");
    }else if (pid == 0){
        printf("子进程, fork 返回值:%d, ID:%d, 父进程ID:%d\n", pid, getpid(), getppid());
    }else {
        printf("父进程, fork 返回值:%d, ID:%d, 父进程ID:%d\n", pid, getpid(), getppid());
    }
    return 0;
}
