#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <unistd.h>
#include <sys/ipc.h>
void msg_show_attr(int msg_id, struct msgid_ds msg_info){
    int ret = -1;
    sleep(1);
    ret = msgctl(msg_id, IPC_STAT, &msg_info);
    if(-1 == ret){
        printf("获取消息信息失败\n");
        return ;
    }
    printf("\n");
    printf("现在队列中的字节数:%d\n", msg_info.msg_cbytes);
    printf("队列中的消息数:%d\n", msg_info.msg_qnum);
    printf("队列中的最大字节数:%d\n", msg_info.msg_qbytes);
    printf("最后发送消息的进程pid:%d\n", msg_info.msg_lspid);

}
